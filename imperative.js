function handleEvent(event) {
  event.things = [];
  
  function doOneThing(cb) {
    stuff("foo", (err, result) => {
      event.things.push(result.prop);
      cb();
    });
  }
  
  function doAnotherThing(cb) {
    stuff("bar", (err, result) => {
      event.things.push(result.otherProp);
      cb();
    })
  }
  
  doOneThing(() => {
    doAnotherThing(() => {
      handler.send(event);
    });
  });
}