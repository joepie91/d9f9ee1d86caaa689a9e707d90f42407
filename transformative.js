function doOneThing() {
  return Promise.try(() => {
    return stuff("foo");
  }).then((result) => {
    return result.prop;
  });
}

function doAnotherThing() {
  return Promise.try(() => {
    return stuff("bar");
  }).then((result) => {
    return result.otherProp;
  });
}


function handleEvent(event) {
  Promise.try(() => {
    return Promise.all([
      doOneThing(),
      doAnotherThing()
    ]);
  }).then((things) => {
    let newEvent = Object.assign({
      things: things
    }, event);
    
    handler.send(newEvent);
  });
}